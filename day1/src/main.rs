fn parse_input(input: &str) -> Vec<u64> {
    let mut nums = Vec::new();
    for l in input.lines() {
        nums.push(l.parse::<u64>().expect("not a integer"));
    }
    nums
}

fn count_inc(nums: &Vec<u64>) -> u64 {
    let mut counter = 0;
    // set to MAX because the first one does not count
    let mut old_n = u64::MAX;
    for n in nums {
        if *n > old_n {
            counter += 1;
        }

        old_n = *n;
    }
    counter
}

fn count_inc_window(nums: &Vec<u64>, sz: usize) -> u64 {
    let window_sums = nums.windows(sz)
        .map(|window| window.iter().sum())
        .collect();
    count_inc(&window_sums)
}

fn main() {
    let nums = parse_input(INPUT);
    let incs = count_inc(&nums);
    println!("incs: {}", incs);
    let incs_window = count_inc_window(&nums, 3);
    println!("incs_window: {}", incs_window);
}

// #include input because i am too lazy to load it
const INPUT: &'static str = include_str!("INPUT.txt");
