fn main() {
    let cmds = parse_commands(INPUT);

    let pos = find_pos(&cmds);
    println!("submarine pos: {:?}", pos);
    println!("\tmultiplied: {}", pos.hpos * pos.depth);

    let pos2 = find_pos_part2(&cmds);
    println!("submarine pos par2: {:?}", pos2);
    println!("\tmultiplied: {}", pos2.hpos * pos2.depth);
}

const INPUT: &'static str = include_str!("INPUT.txt");

enum Command {
    Forward(i64),
    Down(i64),
    Up(i64),
}

#[derive(Debug)]
struct SubmarinePos {
    hpos: i64,
    depth: i64,
    aim: i64,
}

impl SubmarinePos {
    fn new() -> Self {
        Self {
            hpos: 0,
            depth: 0,
            aim: 0,
        }
    }

    fn apply_command(&mut self, cmd: &Command) {
        match *cmd {
            Command::Forward(i) => self.hpos += i,
            Command::Down(i) => self.depth += i,
            Command::Up(i) => self.depth -= i,
        }
    }

    fn apply_command_part2(&mut self, cmd: &Command) {
        match *cmd {
            Command::Forward(i) => {
                self.hpos += i;
                self.depth += self.aim * i;
            },
            Command::Down(i) => self.aim += i,
            Command::Up(i) => self.aim -= i,
        }
    }
}

fn parse_commands(input: &str) -> Vec<Command> {
    let mut output: Vec<Command> = Vec::new();
    for l in input.lines() {
        let (cmd, dist) = l.split_once(" ").expect("not space found");
        let dist: i64 = dist.parse().expect("distance not a i64");
        match cmd {
            "forward" => output.push(Command::Forward(dist)),
            "down" => output.push(Command::Down(dist)),
            "up" => output.push(Command::Up(dist)),
            _ => panic!("unexpected command"),
        }
    }
    output
}

fn find_pos(cmds: &Vec<Command>) -> SubmarinePos {
    let mut pos = SubmarinePos::new();
    for cmd in cmds {
        pos.apply_command(cmd);
    }
    pos
}

fn find_pos_part2(cmds: &Vec<Command>) -> SubmarinePos {
    let mut pos = SubmarinePos::new();
    for cmd in cmds {
        pos.apply_command_part2(cmd);
    }
    pos
}
