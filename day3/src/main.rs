use std::collections::HashSet;

const INPUT: &str = include_str!("INPUT.txt");

fn main() {
    let nums = parse_input(INPUT);
    let (gamma, epsilon) = find_gamma_and_epsilon(&nums);
    println!("{} * {} = {}", gamma, epsilon, gamma * epsilon);
    // i could not solve part2
    //let o2 = find_o2(&nums);
    //let co2 = find_co2(&nums);
    //let co2 = 0;
    //println!("{} * {} = {}", o2, co2, o2 * &co2);
}

fn parse_input(input: &str) -> Vec<u16> {
    let mut output: Vec<u16> = Vec::new();

    for l in input.lines() {
        output.push(u16::from_str_radix(l, 2).expect("expected binary number"));
    }

    output
}

fn find_gamma_and_epsilon(input: &[u16]) -> (u32, u32) {
    let (one_count, zero_count) = count_bits(input);
    let mut gamma: u32 = 0;
    let mut epsilon: u32 = 0;

    for i in 0..zero_count.len() {
        if zero_count[i] > one_count[i] {
            epsilon += 1 << i;
        } else {
            gamma += 1 << i;
        }
    }

    (gamma, epsilon)
}

fn find_o2(input: &[u16]) -> u16 {

    let mut set: HashSet<u16> = HashSet::new();
    for i in input {
        set.insert(*i);
    }

    for i in 0..12 {
        for num in set.clone() /* this is extremely not optimal */ {
            let (one_count, zero_count) = count_bits(&input);
            let filter = (0b1 << i) & num;
            if filter > 0 {
                if !one_count[i] >= zero_count[i] {
                    set.remove(&num);
                }
            } else {
                if !one_count[i] < zero_count[i] {
                    set.remove(&num);
                }
            }
        }
        if set.len() == 1 {
            break;
        }
    }

    set.into_iter().nth(0).unwrap()
}

fn find_co2(input: &[u16]) -> u16 {

    let mut set: HashSet<u16> = HashSet::new();
    for i in input {
        set.insert(*i);
    }

    for i in 0..12 {
        for num in set.clone() /* this is extremely not optimal */ {
            let (one_count, zero_count) = count_bits(&input);
            let filter = (0b1 << 11 - i) & num;
            if one_count[i] < zero_count[i] {
                // onecount is less populer
                if (num & filter) == 0 {
                    set.remove(&num);
                }
            } else {
                if (num & filter) != 0 {
                    set.remove(&num);
                }
            }
        }
        if set.len() == 1 {
            break;
        }
       //dbg!(&set);
       // dbg!(i);
    }


    set.into_iter().nth(0).unwrap()
}

fn count_bits/*<'a, T>*/(input: &[u16]) -> ([u32; 12], [u32; 12])
    //where T: Iterator<Item = &'a u16>
{
    let mut zero_count: [u32; 12] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let mut one_count: [u32; 12] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    for num in input {
        for pos in 0..12 {
            if zero_or_one(*num, pos) {
                one_count[pos] += 1;
            } else {
                zero_count[pos] += 1;
            }
        }
    }

    (zero_count, one_count)
}

fn zero_or_one(int: u16, pos: usize) -> bool {
    let mask = 0b1 << pos;
    (int & mask) > 0
}
