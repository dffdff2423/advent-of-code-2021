use std::collections::HashSet;

const INPUT: &'static str = include_str!("INPUT.txt");

fn main() {
    let mut bingo = parse_input(INPUT);
    println!("num: {}", bingo.find_aoc_number());
    println!("par2 num: {}", bingo.find_par2_number());
}

#[derive(Debug)]
struct Bingo {
    cards: Vec<ScoreCard>,
    nums: Vec<u8>,
}

impl Bingo {
    fn find_aoc_number(&mut self) -> u64 {
        for call in self.nums.iter() {
            for card in self.cards.iter_mut() {
                card.mark_number(*call);
                if card.check() {
                    return card.add_unmarked() * (*call as u64);
                }
            }
        }

        unreachable!();
    }
    fn find_par2_number(&mut self) -> u64 {
        // this is very inefficient
        let mut successes: Vec<(ScoreCard, u8)> = Vec::new();
        let mut skip: HashSet<ScoreCard> = HashSet::new();
        for call in self.nums.iter() {
            for (i, card) in self.cards.iter_mut().enumerate() {
                if skip.contains(&card) {
                    continue;
                }
                card.mark_number(*call);
                if card.check() {
                    //return card.add_unmarked() * (*call as u64);
                    successes.push((card.clone(), *call));
                    skip.insert(card.clone());
                }
            }
        }

        let (last_winner, call) = successes.last().unwrap();
        dbg!(call);
        dbg!(last_winner);
        return dbg!(last_winner.add_unmarked()) * (*call as u64);
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct CardPos {
    num: u8,
    marked: bool,
}

impl From<u8> for CardPos {
    fn from(i: u8) -> Self {
        Self {
            num: i,
            marked: false,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct ScoreCard {
    scores: [CardPos; 25],
}

impl ScoreCard {
    fn from_vec(scores: Vec<u8>) -> Self {
        assert_eq!(scores.len(), 25);

        let scores: Vec<CardPos> = scores.iter()
                .map(|i| CardPos::from(*i))
                .collect();

        Self {
            scores: scores.try_into().unwrap(),
        }
    }

    fn mark_number(&mut self, num: u8) {
        for i in self.scores.iter_mut() {
            if i.num == num {
                i.marked = true;
            }
        }
    }

    fn check(&self) -> bool {
        for i in 0..5 {
            if self.check_row(i) {
                return true;
            }

            if self.check_column(i) {
                return true;
            }
        }
        false
    }

    fn check_row(&self, row: usize) -> bool {
        let off = row * 5;
        let mut res = true;
        for i in 0..5 {
            res = res && self.scores[i + off].marked;
        }

        res
    }

    fn check_column(&self, column: usize) -> bool {
        let column = column + 1;
        let mut res = true;
        for i in 1..6 {
            let index = (i * 5) - (5 - column);
            res = res && self.scores[index - 1].marked;
        }

        res
    }

    fn add_unmarked(&self) -> u64 {
        self.scores.iter().filter(|c| !c.marked).fold(0, |acc, c| acc + c.num as u64)
    }
}

fn parse_input(input: &str) -> Bingo {
    let mut blocks = input.split("\n\n");
    let nums = parse_numbers(blocks.next().unwrap());

    let mut cards: Vec<ScoreCard> = Vec::new();
    for block in blocks {
        let mut nums: Vec<u8> = Vec::new();

        for num in block.split_ascii_whitespace() {
            nums.push(num.parse::<u8>().expect("expected number"));
        }

        cards.push(ScoreCard::from_vec(nums));
    }

    Bingo {
        nums,
        cards,
    }
}

fn parse_numbers(input: &str) -> Vec<u8> {
    let mut nums: Vec<u8> = Vec::new();

    for num in input.split(',') {
        let i = num.parse::<u8>().expect("expected number");
        nums.push(i);
    }

    nums
}
