use std::cmp;

use image::RgbImage;

const INPUT: &'static str = include_str!("INPUT.txt");

fn main() {
    let lines = parse_input(INPUT);

    let mut grid = Grid::new();
    grid.draw_lines(&lines);
    let overlaps = grid.count_overlaps();
    println!("overlaps: {}", overlaps);

    let mut grid2 = Grid::new();
    grid2.draw_lines_part2(&lines);
    let overlaps = grid2.count_overlaps();
    println!("part2 overlaps: {}", overlaps);

    //let img = grid.to_image();
    //img.save_with_format("output.png", image::ImageFormat::Png).unwrap();
}

fn parse_input(input: &str) -> Vec<Line> {
    let mut out = Vec::new();

    for l in input.lines() {
        let (start, end) = l.split_once(" -> ").expect("expected arrow");
        let line = Line {
            start: parse_point(start),
            end: parse_point(end),
        };
        out.push(line);
    }

    out
}

fn parse_point(input: &str) -> Point {
    let (num, num2) = input.split_once(',').expect("expected comma");
    Point {
        x: num.parse().unwrap(),
        y: num2.parse().unwrap(),
    }
}

#[derive(Debug, Clone, Copy)]
struct Point {
    x: u32,
    y: u32,
}

#[derive(Debug, Clone, Copy)]
struct Line {
    start: Point,
    end: Point,
}

#[derive(Debug)]
struct Grid {
    // column major order
    cells: Vec<Vec<u32>>,
}

impl Grid {
    fn new() -> Self {
        let mut dim1 = Vec::new();
        dim1.reserve(1000);
        for _ in 0..1000 {
            let dim2 = vec![0u32; 1000];
            dim1.push(dim2);
        }

        Self {
            cells: dim1,
        }
    }

    fn draw_lines(&mut self, lines: &[Line]) {
        for line in lines {
            let horz = line.start.y == line.end.y;
            let vert = line.start.x == line.end.x;

            if !(horz || vert) {
                //eprintln!("skiping: {:?}", &line);
                continue;
            }

            if horz {
                let min = cmp::min(line.start.x, line.end.x);
                let max = cmp::max(line.start.x, line.end.x) + 1;
                for i in min..max {
                    self.cells[i as usize][line.start.y as usize] += 1;
                }
            }

            if vert {
                let min = cmp::min(line.start.y, line.end.y);
                let max = cmp::max(line.start.y, line.end.y) + 1;
                for i in min..max {
                    self.cells[line.start.x as usize][i as usize] += 1;
                }
            }
        }
    }

    fn draw_lines_part2(&mut self, lines: &[Line]) {
        for line in lines {
            let horz = line.start.y == line.end.y;
            let vert = line.start.x == line.end.x;

            if horz {
                let min = cmp::min(line.start.x, line.end.x);
                let max = cmp::max(line.start.x, line.end.x) + 1;
                for i in min..max {
                    self.cells[i as usize][line.start.y as usize] += 1;
                }
            } else if vert {
                let min = cmp::min(line.start.y, line.end.y);
                let max = cmp::max(line.start.y, line.end.y) + 1;
                for i in min..max {
                    self.cells[line.start.x as usize][i as usize] += 1;
                }
            } else {
                let dirx = if line.start.x < line.end.x {
                    1
                } else {
                    -1
                };

                let diry = if line.start.y < line.end.y {
                    1
                } else {
                    -1
                };

                for i in 0..(i64::abs(line.start.x as i64 - line.end.x as i64) + 1) {
                    let xindex = (line.start.x as i64 + (i * dirx)) as usize;
                    let yindex = (line.start.y as i64 + (i * diry)) as usize;
                    //dbg!(xindex);
                    //dbg!(yindex);
                    self.cells[xindex][yindex] += 1;
                }
            }
        }
    }

    fn count_overlaps(&self) -> u32 {
        let mut counter = 0;
        for vec in &self.cells {
            for cell in vec {
                if *cell >= 2 {
                    counter += 1;
                }
            }
        }

        counter
    }

    #[allow(dead_code)]
    fn to_image(&self) -> RgbImage {
        let mut img = RgbImage::from_pixel(1000, 1000, image::Rgb([255, 255, 255]));
        for (x, y_vec) in self.cells.iter().enumerate() {
            for (y, val) in y_vec.iter().enumerate() {
                img.put_pixel(x as u32, y as u32, image::Rgb([(val * 30) as u8, 100, 100]));
            }
        }
        img
    }
}
