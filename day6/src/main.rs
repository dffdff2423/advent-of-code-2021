const INPUT: &'static str = include_str!("INPUT.txt");

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
struct Fish(u32);

fn main() {
    let mut fishes = parse_input(INPUT);
    let fishes2 = fishes.clone();

    for _ in 0..80 {
        let mut push = Vec::new();
        for fish in fishes.iter_mut() {
            fish.0 -= 1;
            if fish.0 <= 0 {
                fish.0 = 7;
                push.push(Fish(9));
            }
        }

        fishes.append(&mut push);
    }


    let mut timers = [0usize; 9];
    fishes2.iter().for_each(|f| timers[(f.0 - 1) as usize] += 1);
    for i in 0..256 {
        timers.rotate_left(1);
        timers[6] += timers[8];
    }

    println!("fishes: {}", fishes.len());
    println!("fishes2: {}", timers.iter().sum::<usize>());
}

fn parse_input(input: &str) -> Vec<Fish> {
    let mut out = Vec::new();

    for i in input.split(',') {
        out.push(Fish(i.trim_end().parse::<u32>().expect("expected int") + 1));
    }

    out
}
