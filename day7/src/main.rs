const SAMPLE: &'static str = include_str!("SAMPLE.txt");
const INPUT: &'static str = include_str!("INPUT.txt");

fn main() {
    let input = parse_input(INPUT);
    //let input = parse_input(SAMPLE);
    //println!("part1 = {}", part1(&input));
    println!("part2 = {}", part2(&input))
}

fn part1(input: &[u32]) -> i64 {
    let median = median(input);
    let mut counter = 0;
    for i in input {
        counter += i64::abs((median as i64) - (*i as i64));
    }

    counter
}

fn part2(input: &[u32]) -> i64 {
    let mean = mean(input);
    dbg!(mean);
    let mut counter = 0;
    for i in input {
        let dist = i64::abs((mean as i64) - (*i as i64));
        let mut fuel = 0;
        for i in 0..dist + 1 {
            fuel += i;
        }
        //eprintln!("");
        //dbg!(i);
        //dbg!(fuel);
        counter += fuel;
    }

    counter
}

// expects a sorted slice
fn median(slice: &[u32]) -> u32 {
    let len = slice.len();
    let mid = len / 2;

    if len % 2 == 0 {
        return (slice[mid - 1] + slice[mid]) / 2;
    } else {
        return slice[mid];
    }
}

fn mean(slice: &[u32]) -> u64 {
    let sum: u32 = slice.iter().sum();
    // for some reason floor is needed on the input but round is needed on the sample
    return f64::floor(dbg!(sum as f64 / slice.len() as f64)) as u64;
}

fn parse_input(string: &str) -> Vec<u32> {
    let mut output = Vec::new();
    for s in string.split(',') {
        output.push(s.trim().parse().expect("expected int"));
    }
    output.sort();
    output
}
